import { decorate } from './src/core';
import readonly from './src/readonly';

class Person {
  constructor(first, last) {
    this.first = first;
    this.last = last;
  }

  // @readonly
  name() { return `${this.first} ${this.last}` }
}


let person = new Person('Bruno', 'Queiros');
console.log(person.name());

// Descomenta aqui e vai dar o erro do readonly
// person.name = 'Test';
