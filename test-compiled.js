'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _desc, _value, _class;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var description = {
  type: 'method',
  initializer: function initializer() {
    return specifiedFunction;
  },
  enumerable: false,
  configurable: true,
  writable: true
};

description = readonly(Person.prototype, 'name', description) || description;
defineDecoratedProperty(Person.prototype, 'name', description);

function defineDecoratedProperty(target, _ref) {
  var initializer = _ref.initializer;
  var enumerable = _ref.enumerable;
  var configurable = _ref.configurable;
  var writable = _ref.writable;

  Object.defineProperty(target, { value: initializer(), enumerable: enumerable, configurable: configurable, writable: writable });
}

var Person = (_class = (function () {
  function Person() {
    _classCallCheck(this, Person);
  }

  _createClass(Person, [{
    key: 'name',
    value: function name() {
      return this.first + ' ' + this.last;
    }
  }]);

  return Person;
})(), (_desc = readonly(_class.prototype, 'name', _desc = Object.getOwnPropertyDescriptor(_class.prototype, 'name')) || _desc, _desc ? Object.defineProperty(_class.prototype, 'name', _desc) : void 0), _class);
